<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Solo los invitados tendran acceso a esta parte del login
// para que se logueen y gestion sus credenciales get y post
Route::group(['middleware'=>['guest']],function(){
// Referencia raiz me pedira que me loguee
    Route::get('/','Auth\LoginController@showLoginForm');
//Al ponen de colocal las credenciales hara un post para puego procesar los datos
    Route::post('/login','Auth\LoginController@login')->name('login');

});

// Todas las rutas para el usuario autentificado que a su vez tendran rutas segun su rol
Route::group(['middleware'=>['auth']],function(){

    // Solo los usuarios que se loguear tiene derecho a cerrar sesion
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('/main', function () {
        return view('contenido/contenido');
    })->name('main');

    // Rutas solo para el almacenero
    Route::group(['middleware' => ['Almacenero']], function () {
        //Route::resource('categories','CategoriesController');
        Route::get('/categories','CategoriesController@index')->name('categories.index');
        Route::post('/categories','CategoriesController@store')->name('categories.store');
        Route::put('/categories/update','CategoriesController@update')->name('categories.update');
        Route::put('/categories/activar','CategoriesController@activar')->name('categories.activar');
        Route::put('/categories/desactivar','CategoriesController@desactivar')->name('categories.desactivar');
// Creamos una nueva ruta get para poder obtener los categorias
        Route::get('/categories/selectCategoria','CategoriesController@selectCategoria')->name('categories.selectCategoria');


        Route::get('/articles','ArticlesController@index')->name('articles.index');
        Route::post('/articles','ArticlesController@store')->name('articles.store');
        Route::put('/articles/update','ArticlesController@update')->name('articles.update');
        Route::put('/articles/activar','ArticlesController@activar')->name('articles.activar');
        Route::put('/articles/desactivar','ArticlesController@desactivar')->name('articles.desactivar');
        Route::get('/articles/buscarArticulo','ArticlesController@buscarArticulo')->name('articles.buscarArticulo');
        Route::get('/articles/listarArticulo','ArticlesController@listarArticulo')->name('articles.listarArticulo');


        Route::get('/proveedor', 'ProvidersController@index');
        Route::post('/proveedor', 'ProvidersController@store');
        Route::put('/proveedor/update', 'ProvidersController@update');
        Route::get('/proveedor/selectProveedor', 'ProvidersController@selectProveedor');


        Route::get('/ingreso', 'IngresoController@index');
        Route::post('/ingreso','IngresoController@store')->name('ingresos.store');
        Route::put('/ingreso/desactivar','IngresoController@desactivar')->name('ingresos.desactivar');
    });

    // Rutas solo para el Vendedor
    Route::group(['middleware' => ['Vendedor']], function () {

        Route::get('/cliente', 'PersonsController@index');
        Route::post('/cliente', 'PersonsController@store');
        Route::put('/cliente/update', 'PersonsController@update');

    });
    // Todas las rutas es manejado por administrador
    Route::group(['middleware' => ['Administrador']], function () {

        Route::get('/categories','CategoriesController@index')->name('categories.index');
        Route::post('/categories','CategoriesController@store')->name('categories.store');
        Route::put('/categories/update','CategoriesController@update')->name('categories.update');
        Route::put('/categories/activar','CategoriesController@activar')->name('categories.activar');
        Route::put('/categories/desactivar','CategoriesController@desactivar')->name('categories.desactivar');
        // Creamos una nueva ruta get para poder obtener los categorias
        Route::get('/categories/selectCategoria','CategoriesController@selectCategoria')->name('categories.selectCategoria');


        Route::get('/articles','ArticlesController@index')->name('articles.index');
        Route::post('/articles','ArticlesController@store')->name('articles.store');
        Route::put('/articles/update','ArticlesController@update')->name('articles.update');
        Route::put('/articles/activar','ArticlesController@activar')->name('articles.activar');
        Route::put('/articles/desactivar','ArticlesController@desactivar')->name('articles.desactivar');
        Route::get('/articles/buscarArticulo','ArticlesController@buscarArticulo')->name('articles.buscarArticulo');
        Route::get('/articles/listarArticulo','ArticlesController@listarArticulo')->name('articles.listarArticulo');


        Route::get('/proveedor', 'ProvidersController@index');
        Route::post('/proveedor', 'ProvidersController@store');
        Route::put('/proveedor/update', 'ProvidersController@update');
        Route::get('/proveedor/selectProveedor', 'ProvidersController@selectProveedor');


        Route::get('/ingreso', 'IngresoController@index');
        Route::post('/ingreso','IngresoController@store')->name('ingresos.store');
        Route::put('/ingreso/desactivar','IngresoController@desactivar')->name('ingresos.desactivar');


        Route::get('/cliente', 'PersonsController@index');
        Route::post('/cliente', 'PersonsController@store');
        Route::put('/cliente/update', 'PersonsController@update');


        Route::get('/rol', 'RolesController@index');
        Route::get('/rol/selectRol', 'RolesController@selectRol');


        Route::get('/user','UsersController@index')->name('articles.index');
        Route::post('/user','UsersController@store')->name('articles.store');
        Route::put('/user/update','UsersController@update')->name('articles.update');
        Route::put('/user/activar','UsersController@activar')->name('articles.activar');
        Route::put('/user/desactivar','UsersController@desactivar')->name('articles.desactivar');
    });
});

// Route::get('/home', 'HomeController@index')->name('home');
