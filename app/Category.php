<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected  $table = 'categories';
    protected $fillable = [
        'nombre',
        'descripcion',
        'condicion',
    ];

    // 1 articlo pertenece a una categoria
    // 1 categoria  tiene muchos articulos
    public function articles(){
        return $this->hasMany(Article::class);
    }

}
