<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected  $table = 'articles';
    protected $fillable =[
        'idcategory','codigo','nombre','precio_venta','stock','descripcion','condicion'
    ];
    // 1 articlo pertenece a una categoria
    // 1 categoria pertenene tiene muchos articulos
    public function category(){
        return $this->belongsTo(Category::class);
    }
}
