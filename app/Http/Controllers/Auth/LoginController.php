<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
// uso estas 2 libreias para poder hacer uso del login
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function showLoginForm(){
        return view('auth.login');
    }

    public function login(Request $request){
        $this->validateLogin($request);

        // tanto usuario password y la condicion deben ser valores de la tabla user
        if (Auth::attempt(['usuario' => $request->usuario,'password' => $request->password,'condicion'=>1])){
            return redirect()->route('main');
        }
         // si no existe usuario regresa donde estaba
        return back()
            // metodo withErrors recibi un parametro
                // trans para traducir el error y el mensaje lo vas a encontrar
                // resource/lang/en/auth.php y tradces tu mensaje
            ->withErrors(['usuario' => trans('auth.failed')])
            // estte withInput sirve para recuperar el usuario ya que al momento
                // de equivocarse de passwod el nombre del usuario s emantenga en caja
            ->withInput(request(['usuario']));

    }

    protected function validateLogin(Request $request){
        $this->validate($request,[
            'usuario' => 'required|string',
            'password' => 'required|string'
        ]);

    }
    // Para cerrar sesion
    public function logout(Request $request){
        Auth::logout();
        // hago llamo a la cuenta sesion luego invalidate
        $request->session()->invalidate();
        // y me redigira al index
        return redirect('/');
    }


}
