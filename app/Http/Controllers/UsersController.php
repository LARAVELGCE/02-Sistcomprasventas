<?php

namespace App\Http\Controllers;

use App\Person;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    public function index(Request $request)
    {
         if (!$request->ajax()) return redirect('/');

        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if ($buscar==''){
            $persons = User::join('persons','users.id','=','persons.id')
                ->join('roles','users.idrol','=','roles.id')
                ->select('persons.id','persons.nombre','persons.tipo_documento',
                    'persons.num_documento','persons.direccion','persons.telefono',
                    'persons.email','users.usuario','users.password',
                    'users.condicion','users.idrol','roles.nombre as rol')
                ->orderBy('persons.id', 'desc')->paginate(3);
        }
        else{
            $persons = User::join('persons','users.id','=','persons.id')
                ->join('roles','users.idrol','=','roles.id')
                ->select('persons.id','persons.nombre','persons.tipo_documento',
                    'persons.num_documento','persons.direccion','persons.telefono',
                    'persons.email','users.usuario','users.password',
                    'users.condicion','users.idrol','roles.nombre as rol')
                ->where('persons.'.$criterio, 'like', '%'. $buscar . '%')
                ->orderBy('persons.id', 'desc')->paginate(3);
        }


        return [
            'pagination' => [
                'total'        => $persons->total(),
                'current_page' => $persons->currentPage(),
                'per_page'     => $persons->perPage(),
                'last_page'    => $persons->lastPage(),
                'from'         => $persons->firstItem(),
                'to'           => $persons->lastItem(),
            ],
            'persons' => $persons
        ];
    }

    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        try{
            DB::beginTransaction();
            $persons = new Person();
            $persons->nombre = $request->nombre;
            $persons->tipo_documento = $request->tipo_documento;
            $persons->num_documento = $request->num_documento;
            $persons->direccion = $request->direccion;
            $persons->telefono = $request->telefono;
            $persons->email = $request->email;
            $persons->save();

            $user = new User();
            $user->usuario = $request->usuario;
            $user->password = bcrypt( $request->password);
            $user->condicion = '1';
            $user->idrol = $request->idrol;

            $user->id = $persons->id;

            $user->save();

            DB::commit();

        } catch (Exception $e){
            DB::rollBack();
        }



    }

    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        try{
            DB::beginTransaction();

            //Buscar primero el proveedor a modificar
            $user = User::findOrFail($request->id);
            // aqui sera el mismo id que viene desde user
            $persons = Person::findOrFail($user->id);

            $persons->nombre = $request->nombre;
            $persons->tipo_documento = $request->tipo_documento;
            $persons->num_documento = $request->num_documento;
            $persons->direccion = $request->direccion;
            $persons->telefono = $request->telefono;
            $persons->email = $request->email;
            $persons->save();


            $user->usuario = $request->usuario;
            $user->password = bcrypt( $request->password);
            $user->condicion = '1';
            $user->idrol = $request->idrol;
            $user->save();


            DB::commit();

        } catch (Exception $e){
            DB::rollBack();
        }

    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = User::findOrFail($request->id);
        $user->condicion = '0';
        $user->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = User::findOrFail($request->id);
        $user->condicion = '1';
        $user->save();
    }

}
