<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoriesController extends Controller
{

    public function index(Request $request)
    {
        // si la pagina es diferente de ajax(osea pagina que se refrescan)
        // rediriges al index
        if (!$request->ajax()) return redirect('/');
        $buscar = $request->buscar;
        $criterio = $request->criterio;
        if ($buscar==''){

            $categories = Category::orderBy('id','desc')->paginate(3);
        }
        else
        {
            $categories = Category::where($criterio,'like','%'. $buscar .'%')->orderBy('id','desc')->paginate(3);
        }
        return [
            'pagination' => [
                'total'        => $categories->total(),
                'current_page' => $categories->currentPage(),
                'per_page'     => $categories->perPage(),
                'last_page'    => $categories->lastPage(),
                'from'         => $categories->firstItem(),
                'to'           => $categories->lastItem(),
            ],
            'categories' => $categories
        ];
    }
    public function selectCategoria(Request $request){
        // descomentos esto solo para pruebas para que ajax no me redirija al inicio
        if (!$request->ajax()) return redirect('/');
        $categories = Category::where('condicion','=','1')
            ->select('id','nombre')->orderBy('nombre', 'asc')->get();
        return ['categories' => $categories];
    }



    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $categories = new Category();
        $categories->nombre = $request->nombre;
        $categories->descripcion = $request->descripcion;
        $categories->condicion = '1';
        $categories->save();
    }


// Recuerda que aqui no se debe de pasar el id
    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $categories = Category::findOrFail($request->id);
        $categories->nombre = $request->nombre;
        $categories->descripcion = $request->descripcion;
        $categories->condicion = '1';
        $categories->save();
    }


    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $categories = Category::findOrFail($request->id);
        $categories->condicion = '0';
        $categories->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $categories = Category::findOrFail($request->id);
        $categories->condicion = '1';
        $categories->save();
    }

}
