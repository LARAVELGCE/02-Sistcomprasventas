<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;

class ArticlesController extends Controller
{
    public function index(Request $request)
    {
        // si la pagina es diferente de ajax(osea pagina que se refrescan)
        // rediriges al index
         if (!$request->ajax()) return redirect('/');
        $buscar = $request->buscar;
        $criterio = $request->criterio;
        if ($buscar==''){

            $articles = Article::join('categories','articles.id_category','=','categories.id')
            ->select('articles.id','articles.id_category','articles.codigo','articles.nombre','categories.nombre as nombre_category','articles.precio_venta','articles.stock','articles.descripcion','articles.condicion')
            ->orderBy('articles.id','desc')->paginate(3);
        }
        else
        {
            $articles = Article::join('categories','articles.id_category','=','categories.id')
                ->select('articles.id','articles.id_category','articles.codigo','articles.nombre','categories.nombre as nombre_category','articles.precio_venta','articles.stock','articles.descripcion','articles.condicion')
                ->where('articles.'.$criterio,'like','%'. $buscar .'%')
                ->orderBy('articles.id','desc')->paginate(3);

         }
        return [
            'pagination' => [
                'total'        => $articles->total(),
                'current_page' => $articles->currentPage(),
                'per_page'     => $articles->perPage(),
                'last_page'    => $articles->lastPage(),
                'from'         => $articles->firstItem(),
                'to'           => $articles->lastItem(),
            ],
            'articles' => $articles
        ];
    }
    public function buscarArticulo(Request $request){
        if (!$request->ajax()) return redirect('/');

        $filtro = $request->filtro;
        // Codigo sera igual al filtro que le mando
        $articulos = Article::where('codigo','=', $filtro)
            // tomo un solo registro
            ->select('id','nombre')->orderBy('nombre', 'asc')->take(1)->get();

        return ['articulos' => $articulos];
    }
    public function listarArticulo(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if ($buscar==''){
            $articulos = Article::join('categories','articles.id_category','=','categories.id')
                ->select('articles.id','articles.id_category','articles.codigo','articles.nombre','categories.nombre as nombre_categoria','articles.precio_venta','articles.stock','articles.descripcion','articles.condicion')
                ->orderBy('articles.id', 'desc')->paginate(10);
        }
        else{
            $articulos = Article::join('categories','articles.id_category','=','categories.id')
                ->select('articles.id','articles.id_category','articles.codigo','articles.nombre','categories.nombre as nombre_categoria','articles.precio_venta','articles.stock','articles.descripcion','articles.condicion')
                ->where('articles.'.$criterio, 'like', '%'. $buscar . '%')
                ->orderBy('articles.id', 'desc')->paginate(10);
        }


        return ['articulos' => $articulos];
    }



    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $articles = new Article();
        $articles->id_category = $request->id_category;
        $articles->codigo = $request->codigo;
        $articles->nombre = $request->nombre;
        $articles->precio_venta = $request->precio_venta;
        $articles->stock = $request->stock;
        $articles->descripcion = $request->descripcion;
        $articles->condicion = '1';
        $articles->save();
    }


// Recuerda que aqui no se debe de pasar el id
    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $articles = Article::findOrFail($request->id);
        $articles->id_category = $request->id_category;
        $articles->codigo = $request->codigo;
        $articles->nombre = $request->nombre;
        $articles->precio_venta = $request->precio_venta;
        $articles->stock = $request->stock;
        $articles->descripcion = $request->descripcion;
        $articles->condicion = '1';
        $articles->save();
    }


    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $articles = Article::findOrFail($request->id);
        $articles->condicion = '0';
        $articles->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $articles = Article::findOrFail($request->id);
        $articles->condicion = '1';
        $articles->save();
    }
}
