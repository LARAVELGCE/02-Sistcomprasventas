<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ingreso;
use App\DetalleIngreso;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class IngresoController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if ($buscar==''){
            $ingresos = Ingreso::join('persons','ingresos.idproveedor','=','persons.id')
                ->join('users','ingresos.idusuario','=','users.id')
                ->select('ingresos.id','ingresos.tipo_comprobante','ingresos.serie_comprobante',
                    'ingresos.num_comprobante','ingresos.fecha_hora','ingresos.impuesto','ingresos.total',
                    'ingresos.estado','persons.nombre','users.usuario')
                ->orderBy('ingresos.id', 'desc')->paginate(3);
        }
        else{
            $ingresos = Ingreso::join('persons','ingresos.idproveedor','=','persons.id')
                ->join('users','ingresos.idusuario','=','users.id')
                ->select('ingresos.id','ingresos.tipo_comprobante','ingresos.serie_comprobante',
                    'ingresos.num_comprobante','ingresos.fecha_hora','ingresos.impuesto','ingresos.total',
                    'ingresos.estado','persons.nombre','users.usuario')
                ->where('ingresos.'.$criterio, 'like', '%'. $buscar . '%')->orderBy('ingresos.id', 'desc')->paginate(3);
        }

        return [
            'pagination' => [
                'total'        => $ingresos->total(),
                'current_page' => $ingresos->currentPage(),
                'per_page'     => $ingresos->perPage(),
                'last_page'    => $ingresos->lastPage(),
                'from'         => $ingresos->firstItem(),
                'to'           => $ingresos->lastItem(),
            ],
            'ingresos' => $ingresos
        ];
    }

    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        try{
            DB::beginTransaction();
            // Para que mi fecha sea zona Horaria / Peru - Lima
            $mytime= Carbon::now('America/Lima');

            $ingreso = new Ingreso();
            //idproveedor del proveedor
            $ingreso->idproveedor = $request->idproveedor;
            // el idusuario de tabla user
            $ingreso->idusuario = \Auth::user()->id;
            $ingreso->tipo_comprobante = $request->tipo_comprobante;
            $ingreso->serie_comprobante = $request->serie_comprobante;
            $ingreso->num_comprobante = $request->num_comprobante;
            // Convierte a una hora aceptable toDateString()
            $ingreso->fecha_hora = $mytime->toDateString();
            $ingreso->impuesto = $request->impuesto;
            $ingreso->total = $request->total;
            // un estado 'registrado ' para saber que se registro
            $ingreso->estado = 'Registrado';
            $ingreso->save();

            $detalles = $request->data;//Array de detalles
            //Recorro todos los elementos del detalle

            foreach($detalles as $ep=>$det)
            {
                $detalle = new DetalleIngreso();
                // lo alineo a su ID tanto del detalle y de la tabla ingreso
                $detalle->idingreso = $ingreso->id;
                $detalle->idarticulo = $det['idarticulo'];
                $detalle->cantidad = $det['cantidad'];
                $detalle->precio = $det['precio'];
                $detalle->save();
            }

            DB::commit();
        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function desactivar(Request $request)
    {
        // Cuando se anule un ingreso
        if (!$request->ajax()) return redirect('/');
        $ingreso = Ingreso::findOrFail($request->id);
        $ingreso->estado = 'Anulado';
        $ingreso->save();
    }
}


/*
 * DELIMITER //
CREATE TRIGGER tr_updStockIngreso AFTER INSERT ON detalle_ingresos
 FOR EACH ROW BEGIN
 UPDATE articles SET stock = stock + NEW.cantidad
 WHERE articles.id = NEW.idarticulo;
END
//
DELIMITER ;


 *
 *
 *
 * */
