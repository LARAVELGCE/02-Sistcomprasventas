<?php

namespace App\Http\Controllers;
use App\Person;
use App\Provider;

use Illuminate\Http\Request;
// con esta clase me permite usar DB:ALGO
use Illuminate\Support\Facades\DB;
class ProvidersController extends Controller
{
    public function index(Request $request)
    {
        // if (!$request->ajax()) return redirect('/');

        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if ($buscar==''){
            $persons = Provider::join('persons','providers.id','=','persons.id')
                ->select('persons.id','persons.nombre','persons.tipo_documento',
                    'persons.num_documento','persons.direccion','persons.telefono',
                    'persons.email','providers.contacto','providers.telefono_contacto')
                ->orderBy('persons.id', 'desc')->paginate(3);
        }
        else{
            $persons = Provider::join('persons','providers.id','=','persons.id')
                ->select('persons.id','persons.nombre','persons.tipo_documento',
                    'persons.num_documento','persons.direccion','persons.telefono',
                    'persons.email','providers.contacto','providers.telefono_contacto')
                //'persons.'.$criterio ==> aqui le paso el nombre de la tabla y el criterio
                    // con que campo lo filtrare
                ->where('persons.'.$criterio, 'like', '%'. $buscar . '%')
                ->orderBy('persons.id', 'desc')->paginate(3);
        }


        return [
            'pagination' => [
                'total'        => $persons->total(),
                'current_page' => $persons->currentPage(),
                'per_page'     => $persons->perPage(),
                'last_page'    => $persons->lastPage(),
                'from'         => $persons->firstItem(),
                'to'           => $persons->lastItem(),
            ],
            'persons' => $persons
        ];
    }
    public function selectProveedor(Request $request){
        if (!$request->ajax()) return redirect('/');

        $filtro = $request->filtro;
        $proveedores = Provider::join('persons','providers.id','=','persons.id')
            ->where('persons.nombre', 'like', '%'. $filtro . '%')
            ->orWhere('persons.num_documento', 'like', '%'. $filtro . '%')
            ->select('persons.id','persons.nombre','persons.num_documento')
            ->orderBy('persons.nombre', 'asc')->get();

        return ['proveedores' => $proveedores];
    }

    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        try{
            // Usamos  DB::beginTransaction para poder hacer el try
            DB::beginTransaction();
            $persons = new Person();
            $persons->nombre = $request->nombre;
            $persons->tipo_documento = $request->tipo_documento;
            $persons->num_documento = $request->num_documento;
            $persons->direccion = $request->direccion;
            $persons->telefono = $request->telefono;
            $persons->email = $request->email;
            $persons->save();

            // Cuando registro un proveedot tambien registro a una persona
            $proveedor = new Provider();
            $proveedor->contacto = $request->contacto;
            $proveedor->telefono_contacto = $request->telefono_contacto;
            $proveedor->id = $persons->id;
            $proveedor->save();

            DB::commit();

        } catch (Exception $e){
            DB::rollBack();
        }



    }

    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        try{
            DB::beginTransaction();

            //Buscar el id  el proveedor a modificar
            $proveedor = Provider::findOrFail($request->id);
            // Como el id del proveedor es el mismo de la persona
            // se relaciona
            $persons = Person::findOrFail($proveedor->id);

            $persons->nombre = $request->nombre;
            $persons->tipo_documento = $request->tipo_documento;
            $persons->num_documento = $request->num_documento;
            $persons->direccion = $request->direccion;
            $persons->telefono = $request->telefono;
            $persons->email = $request->email;
            $persons->save();


            $proveedor->contacto = $request->contacto;
            $proveedor->telefono_contacto = $request->telefono_contacto;
            $proveedor->save();

            DB::commit();

        } catch (Exception $e){
            // para deshacer la transacion
            DB::rollBack();
        }

    }
}
