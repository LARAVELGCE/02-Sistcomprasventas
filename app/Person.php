<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected  $table = 'persons';
    protected $fillable = ['nombre','tipo_documento','num_documento','direccion','telefono','email'];


    // Una persona esta relacionada
    // directamente a una proveedor
    public function proveedor()
    {
        return $this->hasOne(Provider::class);
    }

    // Una persona esta relacionada
    // directamente a un usuario
    public function user()
    {
        return $this->hasOne(User::class);
    }

}
