<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $table = 'providers';
    protected $fillable = [
        'id', 'contacto', 'telefono_contacto'
    ];

    public $timestamps = false;

    // Un proveedor pertenece a una persona
    public function persona()
    {
        return $this->belongsTo(Person::class);
    }
}
