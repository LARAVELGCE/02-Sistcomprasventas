<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingreso extends Model
{
    protected  $table = 'ingresos';
    protected $fillable = [
        // no les coloco el id del ingreso porque es
        // autoincrement
        'idproveedor',
        'idusuario',
        'tipo_comprobante',
        'serie_comprobante',
        'num_comprobante',
        'fecha_hora',
        'impuesto',
        'total',
        'estado'
    ];
    // Cada ingreso le pertenede a un usuario
    public function usuario()
    {
        return $this->belongsTo(User::class);
    }
    // Cada ingreso le pertenede a un proveedor
    public function proveedor()
    {
        return $this->belongsTo(Providers::class);
    }

}
