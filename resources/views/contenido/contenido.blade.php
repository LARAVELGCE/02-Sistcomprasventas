    @extends('principal')
    @section('contenido')

        @if(Auth::check())
            @if (Auth::user()->idrol == 1)
                <template v-if="menu==0">

                </template>
                <template v-if="menu==1">
                    <category-component></category-component>
                </template>
                <template v-if="menu==2">
                    <article-component></article-component>
                </template>
                <template v-if="menu==3">
                    <ingreso-component></ingreso-component>
                </template>
                <template v-if="menu==4">
                    <proveedor-component></proveedor-component>
                </template>
                <template v-if="menu==5">
                    <h1>Ventas</h1>
                </template>
                <template v-if="menu==6">
                    <person-component></person-component>
                </template>
                <template v-if="menu==7">
                    <user-component></user-component>
                </template>
                <template v-if="menu==8">
                    <rol-component></rol-component>
                </template>
                <template v-if="menu==9">
                    <h1>Reporte de Ingresos</h1>
                </template>
                <template v-if="menu==10">
                    <h1>Reporte de Ventas</h1>
                </template>
                <template v-if="menu==11">
                    <h1>Ayuda</h1>
                </template>
                <template v-if="menu==12">
                    <h1>Acerca</h1>
                </template>
            @elseif (Auth::user()->idrol == 2)
                <template v-if="menu==5">
                    <h1>Ventas</h1>
                </template>
                <template v-if="menu==6">
                    <person-component></person-component>
                </template>
                <template v-if="menu==10">
                    <h1>Reporte de Ventas</h1>
                </template>
                <template v-if="menu==11">
                    <h1>Ayuda</h1>
                </template>
                <template v-if="menu==12">
                    <h1>Acerca</h1>
                </template>
            @elseif (Auth::user()->idrol == 3)
                <template v-if="menu==1">
                    <category-component></category-component>
                </template>
                <template v-if="menu==2">
                    <article-component></article-component>
                </template>
                <template v-if="menu==3">
                    <ingreso-component></ingreso-component>
                </template>
                <template v-if="menu==4">
                    <proveedor-component></proveedor-component>
                </template>
                <template v-if="menu==9">
                    <h1>Reporte de Ingresos</h1>
                </template>
                <template v-if="menu==11">
                    <h1>Ayuda</h1>
                </template>
                <template v-if="menu==12">
                    <h1>Acerca</h1>
                </template>
            @else

            @endif

        @endif





    @endsection