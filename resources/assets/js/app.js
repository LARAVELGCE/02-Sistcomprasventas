
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('category-component', require('./components/Category.vue'));
Vue.component('article-component', require('./components/Article.vue'));
Vue.component('person-component', require('./components/Person.vue'));
Vue.component('proveedor-component', require('./components/Proveedor.vue'));
Vue.component('rol-component', require('./components/Rol.vue'));
Vue.component('user-component', require('./components/User.vue'));
Vue.component('ingreso-component', require('./components/Ingreso.vue'));

const app = new Vue({
    el: '#app',
    // Creamos un variable para nuestro Menu
    data :{
        menu : 0
    }
});
