let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
    1.- Dentro de la carpeta resources/assets/ crea la carpeta  plantilla
        luego un un css y JS dentro de cada uno pega todos los archivos que quieres
        compilar
    2.- Dentro de la carpeta public crea una carpeta css y js si no lo tiene
    3.- Luego de vas al packete packaje.json  y veremos las dependendecias

        "axios": "^0.18",
        "bootstrap": "^4.0.0",
        "popper.js": "^1.12",
        "cross-env": "^5.1",
        "jquery": "^3.2",
        "laravel-mix": "^2.0",
        "lodash": "^4.17.4",
        "vue": "^2.5.7"

    4.- Luego instalamos npm install
    5.- Corremos el comando npm run dev  --> para que nos devuelva compilado

 |

  .js(['resources/assets/js/app.js'],'public/js/app.js');

  Corremos el comando npm run dev  --> para que nos devuelva compilado el app.js dentro del public

 */

mix.styles([
    'resources/assets/plantilla/css/font-awesome.min.css',
    'resources/assets/plantilla/css/simple-line-icons.min.css',
    'resources/assets/plantilla/css/style.css'
], 'public/css/plantilla.css')
    .scripts([
        'resources/assets/plantilla/js/jquery.min.js',
        'resources/assets/plantilla/js/popper.min.js',
        'resources/assets/plantilla/js/bootstrap.min.js',
        'resources/assets/plantilla/js/Chart.min.js',
        'resources/assets/plantilla/js/pace.min.js',
        'resources/assets/plantilla/js/template.js',
        'resources/assets/plantilla/js/sweetalert2.all.js'
    ], 'public/js/plantilla.js')
    .js(['resources/assets/js/app.js'],'public/js/app.js');

// En la linea 49 agregue la libreria que me descargue en la pagina
// https://sweetalert2.github.io/ luego lo descrargue por CDN
// y lo guarde en esa ruta pra que luego me compilara
// luego npm run dev  para qye ejecute