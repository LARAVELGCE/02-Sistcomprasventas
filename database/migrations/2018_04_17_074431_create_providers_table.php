<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('providers', function (Blueprint $table) {
            // este primer campo sera igual al campo id de la tabla persons
            // por eso no le ponemos increment
            $table->integer('id')->unsigned();
            $table->string('contacto',50)->nullable();
            $table->string('telefono_contacto',50)->nullable();
            // Relacionamos de la tabla providers su id con el id (on )de
            // la tabla personas y la eliminacion sera en cascada
            $table->foreign('id')->references('id')->on('persons')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('providers');
    }
}
